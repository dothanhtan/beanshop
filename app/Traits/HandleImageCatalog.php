<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

trait HandleImageCatalog
{
    protected $path = 'upload/category';

    public function verify(Request $request)
    {
        return $request->hasFile('image');
    }

    public function saveImage(Request $request)
    {
        if($this->verify($request))
        {
            $image = $request->file('image');
            $name = 'catalog_'.rand(10,99) . '.' . $image->getClientOriginalExtension();
            $image->move($this->path, $name);
            return $name;
        }
    }

    public function updateImage($request, $currentImage)
    {
        if($this->verify($request))
        {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }

        return $currentImage;
    }
    public function deleteImage($imageName)
    {
        if($imageName && file_exists($this->path .$imageName))
        {
            unlink($this->path .$imageName);
        }
    }
}
