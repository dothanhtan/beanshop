<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImageProduct
{
    protected $path = 'upload/product';

    public function verify($request)
    {
        return $request->has('image');
    }

    public function saveImage($request)
    {
        if($this->verify($request))
        {
            $index = 1;
            foreach ($request->file('image') as $fileImage) {
                $name = 'product_'.rand(10,99).$index++. '.' . $fileImage->getClientOriginalExtension();
                $fileImage->move($this->path, $name);
                return $name;
            }
        }
    }

    public function updateImage($request, $currentImage)
    {
        if($this->verify($request))
        {
            $this->deleteImage($currentImage);

            return $this->saveImage($request);
        }

        return $currentImage;
    }
    public function deleteImage($imageName)
    {
        if($imageName && file_exists($this->path .$imageName))
        {
            unlink($this->path .$imageName);
        }
    }
}
