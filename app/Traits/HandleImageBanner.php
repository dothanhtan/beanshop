<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImageBanner
{
    protected $path = 'upload/banner';

    public function verify($request)
    {
        return $request->has('image');
    }

    public function saveImage($request)
    {
        if($this->verify($request))
        {
            $image = $request->file('image');
            $name = 'banner_'.rand(10,99) . '.' . $image->getClientOriginalExtension();
            $image->move($this->path, $name);
            return $name;
        }
    }

    public function updateImage($request, $currentImage)
    {
        if($this->verify($request))
        {
            $this->deleteImage($currentImage);

            return $this->saveImage($request);
        }

        return $currentImage;
    }
    public function deleteImage($imageName)
    {
        if($imageName && file_exists($this->path .$imageName))
        {
            unlink($this->path .$imageName);
        }
    }
}
