<?php

namespace App\Models;

use App\Traits\HandleImageBanner;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory, HandleImageBanner;

    protected $table = 'banners';

    protected $fillable = ['image', 'title', 'description', 'status'];
}
