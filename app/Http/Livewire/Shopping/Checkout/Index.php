<?php

namespace App\Http\Livewire\Shopping\Checkout;

use App\Mail\PlacedOrderMail;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Livewire\Component;

class Index extends Component
{
    public $cart, $totalProductPrice = 0;

    public $name, $email, $phone, $pin_code, $address, $payment_mode = NULL, $payment_id = NULL;

    protected $listeners = ['validation' => 'validation', 'transaction' => 'paidOnlineOrder'];

    public function validation()
    {
        $this->validate();
    }

    public function rules()
    {
        return [
            'name'      => 'required|string|max:120',
            'email'     => 'required|email|max:120',
            'phone'     => 'required|string|min:10|max:11',
            'pin_code'  => 'required|string|digits:6',
            'address'   => 'required|string|max:500',
        ];
    }

    public function placeOrder()
    {
        $this->validate();
        $order = Order::create([
            'user_id' => Auth::id(),
            'tracking_no' => 'sudo-'.Str::random(10),
            'name' => $this->name,
            'email'=> $this->email,
            'phone'=> $this->phone,
            'pin_code' => $this->pin_code,
            'address' => $this->address,
            'status_message' => 'in progress',
            'payment_mode' => $this->payment_mode,
            'payment_id' => $this->payment_id
        ]);

        foreach ($this->cart as $item) {
            OrderItem::create([
                'order_id' => $order->id,
                'product_id' => $item->product_id,
                'product_color_id' => $item->product_color_id,
                'product_size_id' => $item->product_size_id,
                'quantity' => $item->quantity,
                'price' => $item->product->selling_price
            ]);

            if ($item->product_color_id != NULL and $item->product_size_id != NULL)
            {
                $item->productColor()->where('id', $item->product_color_id)->decrement('quantity', $item->quantity);
                $item->productSize()->where('id', $item->product_size_id)->decrement('quantity', $item->quantity);
            }
            elseif ($item->product_color_id != NULL)
            {
                $item->productColor()->where('id', $item->product_color_id)->decrement('quantity', $item->quantity);
            }
            else
            {
                $item->product()->where('id', $item->product_id)->decrement('quantity', $item->quantity);
            }
        }
        return $order;
    }

    public function codOrder()
    {
        $this->payment_mode = 'Cash On Delivery';
        $codOrder = $this->placeOrder();
        if ($codOrder) {
            Cart::where('user_id', Auth::id())->delete();

            try {
                $order = Order::findOrFail($codOrder->id);
                Mail::to("$order->email")->send(new PlacedOrderMail($order));
            } catch (\Exception $exception) {
                session()->flash('message', 'Something Went Wrong!');
            }

            session()->flash('message', 'Order Placed Successfully!');
            $this->dispatchBrowserEvent('message', [
                'text' => 'Order Placed Successfully!',
                'type' => 'success',
                'status' => 200
            ]);

            return redirect()->route('thank.you');
        } else {
            $this->dispatchBrowserEvent('message', [
                'text' => 'Something Went Wrong!',
                'type' => 'error',
                'status' => 500
            ]);
        }
    }

    public function paidOnlineOrder($value)
    {
        $this->payment_id = $value;
        $this->payment_mode = 'Paid by Paypal';
        $codOrder = $this->placeOrder();
        if ($codOrder) {
            Cart::where('user_id', Auth::id())->delete();

            try {
                $order = Order::findOrFail($codOrder->id);
                Mail::to("$order->email")->send(new PlacedOrderMail($order));
            } catch (\Exception $exception) {
                session()->flash('message', 'Something Went Wrong!');
            }

            session()->flash('message', 'Order Placed Successfully!');
            $this->dispatchBrowserEvent('message', [
                'text' => 'Order Placed Successfully!',
                'type' => 'success',
                'status' => 200
            ]);

            return redirect()->route('thank.you');
        } else {
            $this->dispatchBrowserEvent('message', [
                'text' => 'Something Went Wrong!',
                'type' => 'error',
                'status' => 500
            ]);
        }
    }

    public function totalProductPrice()
    {
        $this->totalProductPrice = 0;
        $this->cart = Cart::where('user_id', Auth::id())->get();
        foreach ($this->cart as $item) {
            $this->totalProductPrice += $item->product->selling_price * $item->quantity;
        }
        return $this->totalProductPrice;
    }

    public function render()
    {
        $this->name = Auth::user()->name;
        $this->email = Auth::user()->email;
        $this->phone = Auth::user()->phone;
        $this->pin_code = Auth::user()->profile->pin_code;
        $this->address = Auth::user()->profile->address;
        $this->totalProductPrice = $this->totalProductPrice();
        return view('livewire.shopping.checkout.index', ['totalProductPrice' => $this->totalProductPrice]);
    }
}
