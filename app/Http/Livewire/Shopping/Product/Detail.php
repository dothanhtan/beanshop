<?php

namespace App\Http\Livewire\Shopping\Product;

use App\Models\Cart;
use App\Models\Wishlist;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Detail extends Component
{
    public $category, $product, $quantityCount = 1, $productColorID, $productSizeID;

    public $productColorQuantity, $productSizeQuantity;

    public function colorSelected(int $productColorID)
    {
        $this->productColorID = $productColorID;
        $productColor = $this->product->productColors->where('id', $productColorID)->first();
        $this->productColorQuantity = $productColor->quantity;

        if ($this->productColorQuantity == 0) {
            $this->productColorQuantity = 'outOfStock';
        }
    }

    public function sizeSelected(int $productSizeID)
    {
        $this->productSizeID = $productSizeID;
        $productSize = $this->product->productSizes->where('id', $productSizeID)->first();
        $this->productSizeQuantity = $productSize->quantity;

        if ($this->productSizeQuantity == 0) {
            $this->productSizeQuantity = 'outOfStock';
        }
    }

    public function decrementQuantity()
    {
        if ($this->quantityCount > 1) {
            $this->quantityCount--;
        }
    }

    public function incrementQuantity()
    {
        if ($this->quantityCount < 10) {
            $this->quantityCount++;
        }
    }

    public function addToCart(int $productID)
    {
        if (Auth::check())
        {
            if ($this->product->where('id', $productID)->where('status', '0')->exists())
            {
                if ($this->product->productColors->count() > 0 and $this->product->productSizes()->count() > 0)
                {
                    if ($this->productColorQuantity != NULL and $this->productSizeQuantity != NULL)
                    {
                        if (Cart::where('user_id', Auth::id())->where('product_id', $productID)
                            ->where('product_color_id', $this->productColorID)
                            ->where('product_size_id', $this->productSizeID)->exists())
                        {
                            $this->dispatchBrowserEvent('message', [
                                'text' => 'Product already added!',
                                'type' => 'success',
                                'status' => 200
                            ]);
                        }
                        else
                        {
                            $productColor = $this->product->productColors()->where('id', $this->productColorID)->first();
                            $productSize = $this->product->productSizes()->where('id', $this->productSizeID)->first();
                            if ($productColor->quantity > 0 and $productSize->quantity > 0)
                            {
                                if ($productColor->quantity > $this->quantityCount and $productSize->quantity > $this->quantityCount)
                                {
                                    Cart::create([
                                        'user_id' => auth()->id(),
                                        'product_id' => $productID,
                                        'product_color_id' => $this->productColorID,
                                        'product_size_id' => $this->productSizeID,
                                        'quantity' => $this->quantityCount
                                    ]);
                                    $this->emit('cartAddedOrUpdated');
                                    $this->dispatchBrowserEvent('message', [
                                        'text' => 'Product has added to cart',
                                        'type' => 'success',
                                        'status' => 200
                                    ]);
                                }
                                else
                                {
                                    $this->dispatchBrowserEvent('message', [
                                        'text' => 'Only '. $productColor->quantity .' product color quantity and '.$productSize->quantity.' product size quantity available!',
                                        'type' => 'warning',
                                        'status' => 200
                                    ]);
                                }
                            }
                            else
                            {
                                $this->dispatchBrowserEvent('message', [
                                    'text' => 'Out Of Stock',
                                    'type' => 'warning',
                                    'status' => 404
                                ]);
                                return false;
                            }
                        }
                    }
                    else
                    {
                        $this->dispatchBrowserEvent('message', [
                            'text' => 'Select your product color and product size',
                            'type' => 'info',
                            'status' => 200
                        ]);
                    }
                }
                elseif ($this->product->productColors()->count() > 0)
                {
                    if ($this->productColorQuantity != NULL)
                    {
                        if (Cart::where('user_id', Auth::id())->where('product_id', $productID)->where('product_color_id', $this->productColorID)->exists())
                        {
                            $this->dispatchBrowserEvent('message', [
                                'text' => 'Product already added!',
                                'type' => 'success',
                                'status' => 200
                            ]);
                        }
                        else
                        {
                            $productColor = $this->product->productColors()->where('id', $this->productColorID)->first();
                            if ($productColor->quantity > 0)
                            {
                                if ($productColor->quantity > $this->quantityCount)
                                {
                                    Cart::create([
                                        'user_id' => auth()->id(),
                                        'product_id' => $productID,
                                        'product_color_id' => $this->productColorID,
                                        'quantity' => $this->quantityCount
                                    ]);
                                    $this->emit('cartAddedOrUpdated');
                                    $this->dispatchBrowserEvent('message', [
                                        'text' => 'Product has added to cart',
                                        'type' => 'success',
                                        'status' => 200
                                    ]);
                                }
                                else
                                {
                                    $this->dispatchBrowserEvent('message', [
                                        'text' => 'Only '. $productColor->quantity .' quantity available!',
                                        'type' => 'warning',
                                        'status' => 200
                                    ]);
                                }
                            }
                            else
                            {
                                $this->dispatchBrowserEvent('message', [
                                    'text' => 'Out Of Stock',
                                    'type' => 'warning',
                                    'status' => 404
                                ]);
                                return false;
                            }
                        }
                    }
                    else
                    {
                        $this->dispatchBrowserEvent('message', [
                            'text' => 'Select your product color',
                            'type' => 'info',
                            'status' => 404
                        ]);
                        return false;
                    }
                }
                else
                {
                    if (Cart::where('user_id', Auth::id())->where('product_id', $productID)->exists()) {
                        $this->dispatchBrowserEvent('message', [
                            'text' => 'Product already added!',
                            'type' => 'warning',
                            'status' => 404
                        ]);
                    }
                    else
                    {
                        if ($this->product->quantity > 0)
                        {
                            if ($this->product->quantity > $this->quantityCount)
                            {
                                Cart::create([
                                    'user_id' => auth()->id(),
                                    'product_id' => $productID,
                                    'quantity' => $this->quantityCount
                                ]);
                                $this->emit('cartAddedOrUpdated');
                                $this->dispatchBrowserEvent('message', [
                                    'text' => 'Product has added to cart',
                                    'type' => 'success',
                                    'status' => 200
                                ]);
                            }
                            else
                            {
                                $this->dispatchBrowserEvent('message', [
                                    'text' => 'Only '. $this->product->quantity .' quantity available!',
                                    'type' => 'warning',
                                    'status' => 404
                                ]);
                            }
                        }
                        else
                        {
                            $this->dispatchBrowserEvent('message', [
                                'text' => 'Out Of Stock',
                                'type' => 'warning',
                                'status' => 404
                            ]);
                            return false;
                        }
                    }

                }
            }
            else
            {
                $this->dispatchBrowserEvent('message', [
                    'text' => 'Product does not exists!',
                    'type' => 'warning',
                    'status' => 404
                ]);
                return false;
            }
        }
        else
        {
            $this->dispatchBrowserEvent('message', [
                'text' => 'Please login to continue!',
                'type' => 'info',
                'status' => 401
            ]);
            return false;
        }
    }

    public function addToWishlist($productID)
    {
        if (Auth::check()) {
            if (Wishlist::where('user_id', auth()->id())->where('product_id', $productID)->exists()) {
//                session()->flash('message', 'Already added to wishlist!');
                $this->dispatchBrowserEvent('message', [
                    'text' => 'Already added to wishlist!',
                    'type' => 'warning',
                    'status' => 409
                ]);
                return false;
            } else {
                Wishlist::create([
                    'user_id' => auth()->id(),
                    'product_id' => $productID
                ]);
                $this->emit('wishlistAddedOrUpdated');
//                session()->flash('message', 'Add to wishlist successfully!');
                $this->dispatchBrowserEvent('message', [
                    'text' => 'Add to wishlist successfully!',
                    'type' => 'success',
                    'status' => 200
                ]);
            }
        } else {
//            session()->flash('message', 'Please login to continue!');
            $this->dispatchBrowserEvent('message', [
                'text' => 'Please login to continue!',
                'type' => 'info',
                'status' => 401
            ]);
            return false;
        }
    }

    public function mount($category, $product)
    {
        $this->category = $category;
        $this->product = $product;
    }

    public function render()
    {
        return view('livewire.shopping.product.detail', [
            'category' => $this->category,
            'product'  => $this->product
        ]);
    }
}
