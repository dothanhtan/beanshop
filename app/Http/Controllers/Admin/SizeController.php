<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Size\SizeFormRequest;
use App\Models\Size;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    /**
     * @var Size $size
     */
    protected Size $size;

    /**
     * Create a new controller instance.
     *
     * @param Size $size
     */
    public function __construct(Size $size)
    {
        $this->size = $size;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sizes = $this->size->latest('id')->get();
        return view('admin.sizes.index', compact('sizes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sizes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SizeFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SizeFormRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['status'] = $request->status ? '1' : '0';
        $size = $this->size->create($validatedData);
        return redirect()->route('sizes.index')->with('message', 'This size: '.$size->name.' has already created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Size  $size
     * @return \Illuminate\Http\Response
     */
    public function show(Size $size)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $size = $this->size->findOrFail($id);
        return view('admin.sizes.edit', compact('size'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SizeFormRequest $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(SizeFormRequest $request, int $id)
    {
        $validatedData = $request->validated();
        $validatedData['status'] = $request->status ? '1' : '0';
        $size = $this->size->findOrFail($id);
        $size->update($validatedData);
        return redirect()->route('sizes.index')->with('message', 'This size : '.$size->name.' has already updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $size = $this->size->findOrFail($id);
        $size->delete();
        return redirect()->route('sizes.index')->with('message', 'This size has already deleted successfully!');
    }
}
