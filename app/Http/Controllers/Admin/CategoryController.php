<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryFormRequest;
use App\Models\Category;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * @var Category $category
     */
    protected Category $category;

    /**
     * Create a new controller instance.
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $categories = $this->category->latest('id')->all();
        return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoryFormRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryFormRequest $request)
    {
        $dataCreated = $request->all();
        $dataCreated['slug'] = Str::slug($request->slug);
        $dataCreated['status'] = $request->status ? '1' : '0';
        $dataCreated['image'] = $this->category->saveImage($request);
        $this->category->create($dataCreated);
        return redirect()->route('categories.index')->with('message', 'Category added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoryFormRequest  $request
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryFormRequest $request, int $id)
    {
        $dataUpdated = $request->all();
        $category = $this->category->findOrFail($id);
        $dataUpdated['slug'] = Str::slug($request->slug);
        $dataUpdated['status'] = $request->status ? '1' : '0';
        $dataUpdated['image'] = $this->category->updateImage($request, $category->image);
        $category->update($dataUpdated);
        return redirect()->route('categories.index')->with('message', 'Category '.$category->name.' updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
