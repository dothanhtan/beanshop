@extends('admin.layouts.app')
@section('content')
    <div class="main-panel">
        <!-- content-wrapper start -->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="d-flex justify-content-between align-items-center">
                                <span class="text-uppercase">Edit Color</span>
                                <a href="{{route('colors.index')}}" class="btn btn-outline-secondary">BACK</a>
                            </h3>
                        </div>
                        <form method="POST" enctype="multipart/form-data" action="{{route('colors.update', $color->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="form-group mb-3">
                                    <label for="name">Color Name</label>
                                    <input class="form-control" id="name" name="name" value="{{old('name') ?? $color->name}}" autofocus>
                                    @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                </div>

                                <div class="form-group mb-3">
                                    <label for="code">Color Code</label>
                                    <input class="form-control" id="code" name="code" value="{{old('code') ?? $color->code}}" autofocus>
                                    @error('code') <span class="text-danger">{{$message}}</span> @enderror
                                </div>

                                <div class="form-group mb-3">
                                    <label for="status">Status</label>
                                    <input type="checkbox" class="form-check" id="status" name="status" @checked($color->status) style="width: 30px;height: 30px;">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-outline-primary">UPDATE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper end -->

        <!-- Start footer -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
                <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
            </span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
                <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
            </span>
            </div>
        </footer>
        <!-- End footer -->
    </div>
@endsection
