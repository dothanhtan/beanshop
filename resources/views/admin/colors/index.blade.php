@extends('admin.layouts.app')
@section('content')
    <div class="main-panel">
        <!-- content-wrapper start -->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{session('message')}}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="d-flex justify-content-between align-items-center mb-0">
                                <span class="text-uppercase fw-bold">Color</span>
                                <a href="{{route('colors.create')}}" class="btn btn-sm btn-outline-primary">Add Color</a>
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Color Name</th>
                                            <th>Color Code</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($colors as $color)
                                            <tr>
                                                <td>
                                                    {{$color->id}}
                                                </td>
                                                <td>
                                                    {{$color->name}}
                                                </td>
                                                <td>
                                                    {{$color->code}}
                                                </td>
                                                <td>
                                                    <span class="badge bg-primary">{{$color->status == 1 ? 'hidden' : 'visible'}}</span>
                                                </td>
                                                <td>
                                                    <a href="{{route('colors.edit', $color->id)}}" class="btn btn-sm btn-outline-warning"><i class="mdi mdi-pencil"></i></a>
                                                    <a href="{{route('colors.destroy', $color->id)}}" class="btn btn-sm btn-outline-danger" onclick="return confirm('Are you sure that you want to delete this data?')"><i class="mdi mdi-delete"></i></a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5">No colors in table</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-4">
                                {{$colors->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper end -->

        <!-- Start footer -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
            <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
        </span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
            <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
        </span>
            </div>
        </footer>
        <!-- End footer -->
    </div>
@endsection
