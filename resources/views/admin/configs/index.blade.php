@extends('admin.layouts.app')
@section('title', 'Config')
@section('content')
    <div class="main-panel">
        <!-- content-wrapper start -->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12">
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{session('message')}}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>
                <div class="col-md-12 grid-margin">
                    <form action="{{route('configs.store')}}" method="POST">
                        @csrf
                        <div class="card mb-3">
                            <div class="card-header bg-primary">
                                <h3 class="text-white mb-0">Website</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="website_name">Website Name</label>
                                        <input type="text" id="website_name" name="website_name" value="{{$config->website_name ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="website_url">Website Url</label>
                                        <input type="text" id="website_url" name="website_url" value="{{$config->website_url ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-12 mb-3 form-group">
                                        <label for="page_title">Page Title</label>
                                        <input type="text" id="page_title" name="page_title" value="{{$config->page_title ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="meta_keyword">Meta Keyword</label>
                                        <textarea type="text" id="meta_keyword" name="meta_keyword" rows="3" class="form-control form-control-sm">{{$config->meta_keyword ?? ''}}</textarea>
                                    </div>
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="meta_description">Meta Description</label>
                                        <textarea type="text" id="meta_description" name="meta_description" rows="3" class="form-control form-control-sm">{{$config->meta_description ?? ''}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <div class="card-header bg-primary">
                                <h3 class="text-white mb-0">Website - Information</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="email1">Email 1</label>
                                        <input type="email" id="email1" name="email1" value="{{$config->email1 ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="email2">Email 2</label>
                                        <input type="email" id="email2" name="email2" value="{{$config->email2 ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="phone1">Phone 1</label>
                                        <input type="tel" id="phone1" name="phone1" value="{{$config->phone1 ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="phone2">Phone 2</label>
                                        <input type="tel" id="phone2" name="phone2" value="{{$config->phone2 ?? ''}}" class="form-control form-control-sm">
                                    </div>

                                    <div class="col-md-12 mb-3 form-group">
                                        <label for="address">Address</label>
                                        <textarea id="address" name="address" rows="3" class="form-control form-control-sm">{{$config->address ?? ''}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <div class="card-header bg-primary">
                                <h3 class="text-white mb-0">Website - Social Media</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="facebook">Facebook (Optional)</label>
                                        <input type="text" id="facebook" name="facebook" value="{{$config->facebook ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="twitter">Twitter (Optional)</label>
                                        <input type="text" id="twitter" name="twitter" value="{{$config->twitter ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="instagram">Instagram (Optional)</label>
                                        <input type="text" id="instagram" name="instagram" value="{{$config->instagram ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-6 mb-3 form-group">
                                        <label for="youtube">Youtube (Optional)</label>
                                        <input type="text" id="youtube" name="youtube" value="{{$config->youtube ?? ''}}" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Save Settings</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- content-wrapper end -->

        <!-- Start footer -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
                <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
            </span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
                <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
            </span>
            </div>
        </footer>
        <!-- End footer -->
    </div>
@endsection
