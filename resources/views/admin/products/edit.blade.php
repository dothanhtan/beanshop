@extends('admin.layouts.app')
@section('content')
    <div class="main-panel">
        <!-- content-wrapper start -->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{session('message')}}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>

                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="d-flex justify-content-between align-items-center">
                                <span class="text-uppercase">Edit Product</span>
                                <a href="{{route('products.index')}}" class="btn btn-outline-secondary">BACK</a>
                            </h3>
                        </div>
                        @if($errors->any())
                            <div class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    <div>{{$error}}</div>
                                @endforeach
                            </div>
                        @endif
                        <form method="POST" enctype="multipart/form-data" action="{{route('products.update', $product->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">HOME</button>
                                        <button class="nav-link" id="nav-seo-tab" data-bs-toggle="tab" data-bs-target="#nav-seo" type="button" role="tab" aria-controls="nav-seo" aria-selected="false">SEO TAG</button>
                                        <button class="nav-link" id="nav-detail-tab" data-bs-toggle="tab" data-bs-target="#nav-detail" type="button" role="tab" aria-controls="nav-detail" aria-selected="false">DETAILS</button>
                                        <button class="nav-link" id="nav-image-tab" data-bs-toggle="tab" data-bs-target="#nav-image" type="button" role="tab" aria-controls="nav-image" aria-selected="false">PRODUCT IMAGE</button>
                                        <button class="nav-link" id="nav-color-tab" data-bs-toggle="tab" data-bs-target="#nav-color" type="button" role="tab" aria-controls="nav-color" aria-selected="false">PRODUCT COLOR</button>
                                        <button class="nav-link" id="nav-size-tab" data-bs-toggle="tab" data-bs-target="#nav-size" type="button" role="tab" aria-controls="nav-size" aria-selected="false">PRODUCT SIZE</button>
                                    </div>
                                </nav>

                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                        <div class="row">
                                            <div class="col-6 form-group my-3">
                                                <label for="category_id">Select Category</label>
                                                <select id="category_id" name="category_id" class="form-control">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}" @selected($product->category_id == $category->id)>{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                                @error('category_id') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>

                                            <div class="col-6 form-group my-3">
                                                <label for="brand">Select Brand</label>
                                                <select id="brand" name="brand" class="form-control">
                                                    @foreach($brands as $brand)
                                                        <option value="{{$brand->name}}" @selected($product->brand == $brand->name)>{{$brand->name}}</option>
                                                    @endforeach
                                                </select>
                                                @error('brand') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>

                                            <div class="col-6 form-group mb-3">
                                                <label for="name">Name</label>
                                                <input class="form-control" id="name" name="name" value="{{old('name') ?? $product->name}}" autofocus>
                                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>

                                            <div class="col-6 form-group mb-3">
                                                <label for="slug">Slug</label>
                                                <input class="form-control" id="slug" name="slug" value="{{old('slug') ?? $product->slug}}" autofocus>
                                                @error('slug') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>

                                            <div class="col-12 form-group mb-3">
                                                <label for="short_description">Short Description</label>
                                                <textarea class="form-control" id="short_description" name="short_description" rows="3">{{old('short_description') ?? $product->short_description}}</textarea>
                                                @error('short_description') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>

                                            <div class="col-12 form-group mb-3">
                                                <label for="description">Description</label>
                                                <textarea class="form-control" id="description" name="description" rows="5">{{old('description') ?? $product->description}}</textarea>
                                                @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="nav-seo" role="tabpanel" aria-labelledby="nav-seo-tab">
                                        <div class="row">
                                            <div class="col-6 form-group my-3">
                                                <label for="meta_title">Meta title</label>
                                                <input type="text" class="form-control" id="meta_title" name="meta_title" value="{{old('meta_title') ?? $product->meta_title}}">
                                                @error('meta_title') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>

                                            <div class="col-6 form-group my-3">
                                                <label for="meta_keyword">Meta keyword</label>
                                                <input type="text" class="form-control" id="meta_keyword" name="meta_keyword" value="{{old('meta_keyword') ?? $product->meta_keyword}}">
                                                @error('meta_keyword') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>

                                            <div class="col-12 form-group mb-3">
                                                <label for="meta_description">Meta description</label>
                                                <textarea class="form-control" id="meta_description" name="meta_description" rows="3">{{old('meta_description') ?? $product->meta_description}}</textarea>
                                                @error('meta_description') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="nav-detail" role="tabpanel" aria-labelledby="nav-detail-tab">
                                        <div class="row">
                                            <div class="col-4 form-group my-3">
                                                <label for="original_price">Original Price</label>
                                                <input type="number" class="form-check" id="original_price" name="original_price" min="10" value="{{old('original_price') ?? $product->original_price}}">
                                            </div>
                                            <div class="col-4 form-group my-3">
                                                <label for="selling_price">Selling Price</label>
                                                <input type="number" class="form-check" id="selling_price" name="selling_price" min="10" value="{{old('selling_price') ?? $product->selling_price}}">
                                            </div>
                                            <div class="col-4 form-group my-3">
                                                <label for="quantity">Quantity</label>
                                                <input type="number" class="form-check" id="quantity" name="quantity" min="0" value="{{old('quantity') ?? $product->quantity}}">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4 form-group mb-3">
                                                <label for="trending">Trending</label>
                                                <input type="checkbox" class="form-check" @checked($product->trending) id="trending" name="trending" style="width: 30px; height: 30px">
                                            </div>

                                            <div class="col-4 form-group mb-3">
                                                <label for="featured">Featured</label>
                                                <input type="checkbox" class="form-check" @checked($product->featured) id="featured" name="featured" style="width: 30px; height: 30px">
                                            </div>

                                            <div class="col-4 form-group mb-3">
                                                <label for="status">Status</label>
                                                <input type="checkbox" class="form-check" @checked($product->status) id="status" name="status" style="width: 30px; height: 30px">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="nav-image" role="tabpanel" aria-labelledby="nav-image-tab">
                                        <div class="row">
                                            <div class="col-12 form-group my-3">
                                                <label for="image">Upload Product Image</label>
                                                <input type="file" class="form-control" id="image" name="image[]" multiple value="{{old('image')}}">
                                                @error('image') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>

                                        <div class="row">
                                            @forelse($product->productImages as $item)
                                                <div class="col-2">
                                                    <img src="{{asset($item->image)}}" alt="Product Image" class="border" width="100" height="100">
                                                    <a href="{{route('products.remove', $item->id)}}" class="btn btn-block btn-link">Remove</a>
                                                </div>
                                            @empty
                                                <h4>No images uploaded</h4>
                                            @endforelse
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="nav-color" role="tabpanel" aria-labelledby="nav-color-tab">
                                        <div class="col-12 my-3">
                                            <label>Select Color</label>
                                            <div class="border-bottom mt-2"></div>
                                            <div class="row">
                                                @forelse($colors as $color)
                                                    <div class="col-3">
                                                        <div class="border mt-3 p-3">
                                                            <div>
                                                                <label style="float: left; width: 50%">Color</label>
                                                                <span style="float:left; width: 50%;"><input type="checkbox" id="colors" name="colors[{{$color->id}}]" value="{{$color->id}}"> {{$color->name}}</span>
                                                                <div style="clear: both"></div>
                                                            </div>

                                                            <div class="mt-2">
                                                                <label style="float: left; width: 50%; line-height: 25px">Quantity</label>
                                                                <input type="number" name="color_quantity[{{$color->id}}]" style="float:left; width: 50%;">
                                                                <div style="clear: both"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @empty
                                                    <div class="col-12">
                                                        <h3>No colors found</h3>
                                                    </div>
                                                @endforelse
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table table-sm table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Color Name</th>
                                                            <th>Color Quantity</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse($product->productColors as $itemColor)
                                                            <tr class="product-color-row">
                                                                <td>
                                                                    {{$itemColor->color ? $itemColor->color->name : 'No colors found'}}
                                                                </td>
                                                                <td>
                                                                    <div class="input-group w-50">
                                                                        <input type="number" class="form-control form-control-sm input-color-quantity" value="{{$itemColor->quantity}}">
                                                                        <button type="button" class="btn btn-sm btn-primary btn-update-color-quantity" value="{{$itemColor->id}}">Update</button>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <button type="button" value="{{$itemColor->id}}" class="btn btn-sm btn-danger btn-delete-color">Delete</button>
                                                                </td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td colspan="3">No product color found</td>
                                                            </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="nav-size" role="tabpanel" aria-labelledby="nav-size-tab">
                                        <div class="col-12 my-3">
                                            <label>Select Size</label>
                                            <div class="border-bottom mt-2"></div>
                                            <div class="row">
                                                @forelse($sizes as $size)
                                                    <div class="col-3">
                                                        <div class="border mt-3 p-3">
                                                            <div>
                                                                <label style="float: left; width: 50%">Size</label>
                                                                <span style="float:left; width: 50%;"><input type="checkbox" id="sizes" name="sizes[{{$size->id}}]" value="{{$size->id}}"> {{$size->name}}</span>
                                                                <div style="clear: both"></div>
                                                            </div>

                                                            <div class="mt-2">
                                                                <label style="float: left; width: 50%; line-height: 25px">Quantity</label>
                                                                <input type="number" name="size_quantity[{{$size->id}}]" style="float:left; width: 50%;">
                                                                <div style="clear: both"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @empty
                                                    <div class="col-12">
                                                        <h3>No sizes found</h3>
                                                    </div>
                                                @endforelse
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table table-sm table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Size Name</th>
                                                            <th>Size Quantity</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @forelse($product->productSizes as $itemSize)
                                                        <tr class="product-size-row">
                                                            <td>
                                                                {{$itemSize->size ? $itemSize->size->name : 'No sizes found'}}
                                                            </td>
                                                            <td>
                                                                <div class="input-group w-50">
                                                                    <input type="number" class="form-control form-control-sm input-size-quantity" value="{{$itemSize->quantity}}">
                                                                    <button type="button" class="btn btn-sm btn-primary btn-update-size-quantity" value="{{$itemSize->id}}">Update</button>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <button type="button" value="{{$itemSize->id}}" class="btn btn-sm btn-danger btn-delete-size">Delete</button>
                                                            </td>
                                                        </tr>
                                                    @empty
                                                        <tr>
                                                            <td colspan="3">No product size found</td>
                                                        </tr>
                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-outline-primary">UPDATE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper end -->

        <!-- Start footer -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
            <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
        </span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
            <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
        </span>
            </div>
        </footer>
        <!-- End footer -->
    </div>
@endsection

@section('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click', '.btn-update-color-quantity', function () {
            let product_id = "{{$product->id}}"
            let product_color_id = $(this).val()
            let product_color_quantity = $(this).closest('.product-color-row').find('.input-color-quantity').val()

            if (product_color_quantity <= 0) {
                alert('Quantity is required!')
                return false
            }

            let data = {
                'product_id': product_id,
                'product_color_id': product_color_id,
                'product_color_quantity': product_color_quantity
            }
            $.ajax({
                type: "POST",
                url: "/admin/products/"+product_color_id+"/color",
                data: data,
                success: function (res) {
                    alert(res.message)
                }
            })
        })

        $(document).on('click', '.btn-update-size-quantity', function () {
            let product_id = "{{$product->id}}"
            let product_size_id = $(this).val()
            let product_size_quantity = $(this).closest('.product-size-row').find('.input-size-quantity').val()

            if (product_size_quantity <= 0) {
                alert('Quantity is required!')
                return false
            }

            let data = {
                'product_id': product_id,
                'product_size_id': product_size_id,
                'product_size_quantity': product_size_quantity
            }
            $.ajax({
                type: "POST",
                url: "/admin/products/"+product_size_id+"/size",
                data: data,
                success: function (res) {
                    alert(res.message)
                }
            })
        })

        $(document).on('click', '.btn-delete-color', function () {
            let product_color_id = $(this).val()
            let _this = $(this)

            $.ajax({
                type: "GET",
                url: "/admin/products/"+product_color_id+"/color/delete",
                success: function (res) {
                    _this.closest('.product-color-row').remove()
                    alert(res.message)
                }
            })
        })

        $(document).on('click', '.btn-delete-size', function () {
            let product_size_id = $(this).val()
            let _this = $(this)

            $.ajax({
                type: "GET",
                url: "/admin/products/"+product_size_id+"/size/delete",
                success: function (res) {
                    _this.closest('.product-size-row').remove()
                    alert(res.message)
                }
            })
        })
    </script>
@endsection
