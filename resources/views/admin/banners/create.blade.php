@extends('admin.layouts.app')
@section('content')
    <div class="main-panel">
        <!-- content-wrapper start -->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="d-flex justify-content-between align-items-center">
                                <span class="text-uppercase">Add Banner</span>
                                <a href="{{route('banners.index')}}" class="btn btn-outline-secondary">BACK</a>
                            </h3>
                        </div>
                        <form method="POST" enctype="multipart/form-data" action="{{route('banners.store')}}">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 form-group mb-3">
                                        <label for="title">Title</label>
                                        <input class="form-control" id="title" name="title" value="{{old('title')}}" autofocus>
                                        @error('title') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="col-12 form-group mb-3">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" id="description" name="description" rows="3">{{old('description')}}</textarea>
                                        @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="col-6 form-group mb-3">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control" id="image" name="image" value="{{old('image')}}">
                                        @error('image') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="col-6 form-group mb-3">
                                        <label for="status">Status</label>
                                        <input type="checkbox" class="d-block" id="status" name="status" style="width: 44px; height: 44px">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-outline-primary">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper end -->

        <!-- Start footer -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
                <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
            </span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
                <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
            </span>
            </div>
        </footer>
        <!-- End footer -->
    </div>
@endsection
