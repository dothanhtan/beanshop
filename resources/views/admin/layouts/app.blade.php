<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title Website -->
    <title>@yield('title', 'Administrator')</title>

    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('admin/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/base/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">

    <!-- Icon Website -->
    <link rel="icon" href="{{asset('admin/images/microsoft.ico')}}" type="image/x-icon" crossorigin="anonymous" sizes="16x16">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">

    @livewireStyles
</head>

<body>
    <div class="container-scroller">
        <!-- partial:partials -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            @include('admin.layouts.brand')
            @include('admin.layouts.menu')
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            @include('admin.layouts.sidebar')
            @yield('content')
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    @include('admin.layouts.script')

    @yield('script')

    @livewireScripts

    @stack('script')
</body>

</html>
