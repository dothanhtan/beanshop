<!-- Create Brand Modal -->
<div wire:ignore.self class="modal fade" id="addBrandModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Brand</h5>
                <button type="button" class="btn-close" wire:click="closeModal" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <form wire:submit.prevent="storeBrand()">
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label for="category_id">Select Category</label>
                        <select name="category_id" id="category_id" wire:model.defer="category_id" class="form-control">
                            <option>--Select Category--</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        @error('category_id') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group mb-3">
                        <label for="brand_name">Brand Name</label>
                        <input class="form-control" id="brand_name" name="name" wire:model.defer="name" value="{{old('name')}}" autofocus>
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group mb-3">
                        <label for="brand_slug">Brand Slug</label>
                        <input class="form-control" id="brand_slug" name="slug" wire:model.defer="slug" value="{{old('slug')}}" autofocus>
                        @error('slug') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="brand_status">Brand Status</label>
                        <div>
                            <input type="checkbox" class="form-check-input" id="brand_status" name="status" wire:model.defer="status"/>
                            <label for="brand_status" class="form-check-label"> Checked=Hidden, Uncheck=Visible</label>
                        </div>
                        @error('status') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" wire:click="closeModal" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update Brand Modal -->
<div wire:ignore.self class="modal fade" id="editBrandModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Brand</h5>
                <button type="button" class="btn-close" wire:click="closeModal" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div wire:loading class="text-center">
                <div class="spinner-border text-primary" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div> Loading...
            </div>
            <div wire:loading.remove>
                <form wire:submit.prevent="updateBrand()">
                    <div class="modal-body">
                        <div class="form-group mb-3">
                            <label for="category_id">Select Category</label>
                            <select name="category_id" id="category_id" wire:model.defer="category_id" class="form-control">
                                <option>--Select Category--</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            @error('category_id') <span class="text-danger">{{$message}}</span> @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="brand_name">Brand Name</label>
                            <input class="form-control" id="brand_name" name="name" wire:model.defer="name" value="{{old('name')}}" autofocus>
                            @error('name') <span class="text-danger">{{$message}}</span> @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="brand_slug">Brand Slug</label>
                            <input class="form-control" id="brand_slug" name="slug" wire:model.defer="slug" value="{{old('slug')}}" autofocus>
                            @error('slug') <span class="text-danger">{{$message}}</span> @enderror
                        </div>

                        <div class="form-group">
                            <label for="brand_status">Brand Status</label>
                            <div>
                                <input type="checkbox" class="form-check-input" id="brand_status" name="status" wire:model.defer="status"/>
                                <label for="brand_status" class="form-check-label"> Checked=Hidden, Uncheck=Visible</label>
                            </div>
                            @error('status') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-secondary" wire:click="closeModal" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Delete Brand Modal -->
<div wire:ignore.self class="modal fade" id="deleteBrandModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Brand</h5>
                <button type="button" class="btn-close" wire:click="closeModal" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div wire:loading class="text-center">
                <div class="spinner-border text-primary" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div> Loading...
            </div>
            <div wire:loading.remove>
                <form wire:submit.prevent="removeBrand()">
                    <div class="modal-body">
                        <h4>Are you sure that you want to remove this data?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Yes. Delete</button>
                        <button type="button" class="btn btn-secondary" wire:click="closeModal" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
