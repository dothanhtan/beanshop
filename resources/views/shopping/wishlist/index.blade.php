@extends('layouts.app')

@section('title', 'Wishlist')

@section('content')
    <livewire:shopping.wishlist.index />
@endsection
