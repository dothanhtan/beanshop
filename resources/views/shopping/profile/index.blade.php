@extends('layouts.app')

@section('title', 'User Profile')

@section('content')
    <div class="py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <h4 class="d-flex justify-content-between align-items-center">
                        <span class="fw-bold">User Profile</span>
                        <a href="{{route('password.create')}}" class="btn btn-outline-info">Change Password</a>
                    </h4>
                    <div class="underline mb-4"></div>
                </div>
                <div class="col-md-10">
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{session('message')}}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    @if($errors->any())
                        <ul class="alert alert-warning" style="padding-left: 2rem">
                            @foreach($errors->all() as $error)
                                <li class="text-warning">{{$error}}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <div class="col-md-10">
                    <div class="card shadow">
                        <div class="card-body">
                            <form action="{{route('profile.store')}}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="username" class="mb-1">Username</label>
                                        <input type="text" id="username" name="username" value="{{Auth::user()->name}}" class="form-control">
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="email" class="mb-1">Email</label>
                                        <input type="email" id="email" name="email" value="{{Auth::user()->email}}" class="form-control" readonly>
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="phone" class="mb-1">Phone Number</label>
                                        <input type="tel" id="phone" name="phone" value="{{Auth::user()->phone}}" class="form-control" readonly>
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="pin_code" class="mb-1">Pin Code</label>
                                        <input type="number" id="pin_code" name="pin_code" value="{{Auth::user()->profile->pin_code ?? ''}}" class="form-control">
                                    </div>

                                    <div class="col-md-12 form-group mb-3">
                                        <label for="address" class="mb-1">Address</label>
                                        <textarea name="address" id="address" rows="3" class="form-control">{{Auth::user()->profile->address ?? ''}}</textarea>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <button type="submit" class="btn btn-outline-primary">Save Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
